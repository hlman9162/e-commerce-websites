CREATE OR REPLACE PACKAGE pkg_e_commerce_website AS
  PROCEDURE insert_product(
    p_product_name      products.product_name%type,
    p_product_category  products.product_category%type,
    p_product_size      products.product_size%type,
    p_product_stock     products.product_stock%type,
    p_product_price     products.product_price%type,
    p_order_id          orders_log.order_id%type);


  PROCEDURE update_product(
    p_product_name      products.product_name%type,
    p_product_new_price products.product_price%type,
    p_product_new_stock products.product_stock%type,
    p_product_updated   products.product_updated%type,
    p_order_id          orders_log.order_id%type);   
    
    
    
  PROCEDURE delete_product(
    p_product_name      products.product_name%type);
    
    
    
  PROCEDURE get_products_by_category(
    p_product_name      products.product_name%type);

END pkg_e_commerce_website;
/
