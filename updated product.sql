  PROCEDURE update_product(
    p_product_name      products.product_name%type,
    p_product_new_price products.product_price%type,
    p_product_new_stock products.product_stock%type,
    p_product_updated   products.product_updated%type,
    p_order_id          orders_log.order_id%type)  IS
  BEGIN
     UPDATE products
      SET 
         product_price=p_product_new_price,
         product_stock=p_product_new_stock
      WHERE 
         product_name=p_product_name;
         
    INSERT INTO products(product_updated)VALUES(systimestamp);
    INSERT INTO orders_log(
      order_id,
      log_timestamp,
      log_message)
    VALUES(
      p_order_id,
      systimestamp,
      'Product Updated');
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error:'||SQLERRM);
      ROLLBACK;
  END update_product;
