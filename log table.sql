CREATE TABLE orders_log(
 order_id         number,
 log_timestamp    timestamp,
 log_message      varchar2(100));
