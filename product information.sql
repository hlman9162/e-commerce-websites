PROCEDURE get_products_by_category(
    p_product_name      products.product_name%type) IS
  BEGIN
    FOR product_rec IN (SELECT * FROM products WHERE product_name = p_product_name) LOOP
        DBMS_OUTPUT.PUT_LINE('Product Information:');
        DBMS_OUTPUT.PUT_LINE('Product_id:' || product_rec.id);
        DBMS_OUTPUT.PUT_LINE('Product Name:' || product_rec.product_name);
        DBMS_OUTPUT.PUT_LINE('Product Category:' || product_rec.product_category);
        DBMS_OUTPUT.PUT_LINE('Product Size:' || product_rec.product_size);
        DBMS_OUTPUT.PUT_LINE('Product Stock:' || product_rec.product_stock);
        DBMS_OUTPUT.PUT_LINE('Product Price: ' || product_rec.product_price);
        DBMS_OUTPUT.PUT_LINE('Product Inserted:' || product_rec.product_created);
        DBMS_OUTPUT.PUT_LINE('Product Updated:' || product_rec.product_updated);
        DBMS_OUTPUT.PUT_LINE('---');
    END LOOP;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Product Not Found'||p_product_name);
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
  END get_products_by_category;
