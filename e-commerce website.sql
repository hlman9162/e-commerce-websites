--E-commerce websites
DROP TABLE products;
CREATE TABLE products(
 id               number GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
 product_name     varchar2(100) NOT NULL,
 product_category varchar2(100) NOT NULL,
 product_size     varchar2(10),
 product_stock    number,
 product_price    number,
 product_created  timestamp,
 product_updated  timestamp);
 
CREATE TABLE orders_log(
 order_id         number,
 log_timestamp    timestamp,
 log_message      varchar2(100));
 
--PROCEDURE NAME:
--insert product
--update product
--delete product
--get products by category

CREATE OR REPLACE PACKAGE pkg_e_commerce_website AS
  PROCEDURE insert_product(
    p_product_name      products.product_name%type,
    p_product_category  products.product_category%type,
    p_product_size      products.product_size%type,
    p_product_stock     products.product_stock%type,
    p_product_price     products.product_price%type,
    p_order_id          orders_log.order_id%type);


  PROCEDURE update_product(
    p_product_name      products.product_name%type,
    p_product_new_price products.product_price%type,
    p_product_new_stock products.product_stock%type,
    p_product_updated   products.product_updated%type,
    p_order_id          orders_log.order_id%type);   
    
    
    
  PROCEDURE delete_product(
    p_product_name      products.product_name%type);
    
    
    
  PROCEDURE get_products_by_category(
    p_product_name      products.product_name%type);

END pkg_e_commerce_website;
/

CREATE OR REPLACE PACKAGE BODY pkg_e_commerce_website AS
  PROCEDURE insert_product(
    p_product_name      products.product_name%type,
    p_product_category  products.product_category%type,
    p_product_size      products.product_size%type,
    p_product_stock     products.product_stock%type,
    p_product_price     products.product_price%type,
    p_order_id          orders_log.order_id%type) IS
  BEGIN
    INSERT INTO products (
      product_name,
      product_category,
      product_size,
      product_stock,
      product_price,
      product_created)
    VALUES (
      p_product_name,
      p_product_category,
      p_product_size,
      p_product_stock,
      p_product_price,
      SYSTIMESTAMP);
    INSERT INTO orders_log (
      order_id,
      log_timestamp,
      log_message)
    VALUES (
      p_order_id,
      systimestamp,
      'Product Inserted');
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error:' || SQLERRM);
      ROLLBACK;
  END insert_product;

  PROCEDURE update_product(
    p_product_name      products.product_name%type,
    p_product_new_price products.product_price%type,
    p_product_new_stock products.product_stock%type,
    p_product_updated   products.product_updated%type,
    p_order_id          orders_log.order_id%type)  IS
  BEGIN
     UPDATE products
      SET 
         product_price=p_product_new_price,
         product_stock=p_product_new_stock
      WHERE 
         product_name=p_product_name;
         
    INSERT INTO products(product_updated)VALUES(systimestamp);
    INSERT INTO orders_log(
      order_id,
      log_timestamp,
      log_message)
    VALUES(
      p_order_id,
      systimestamp,
      'Product Updated');
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error:'||SQLERRM);
      ROLLBACK;
  END update_product;

  PROCEDURE delete_product(
    p_product_name      products.product_name%type) IS
  BEGIN
     DELETE FROM products WHERE product_name=p_product_name;
   DBMS_OUTPUT.PUT_LINE('Deleted Product');
   COMMIT;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Product Not Found');
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error:' || SQLERRM);
      ROLLBACK;
  END delete_product;

  PROCEDURE get_products_by_category(
    p_product_name      products.product_name%type) IS
  BEGIN
    FOR product_rec IN (SELECT * FROM products WHERE product_name = p_product_name) LOOP
        DBMS_OUTPUT.PUT_LINE('Product Information:');
        DBMS_OUTPUT.PUT_LINE('Product_id:' || product_rec.id);
        DBMS_OUTPUT.PUT_LINE('Product Name:' || product_rec.product_name);
        DBMS_OUTPUT.PUT_LINE('Product Category:' || product_rec.product_category);
        DBMS_OUTPUT.PUT_LINE('Product Size:' || product_rec.product_size);
        DBMS_OUTPUT.PUT_LINE('Product Stock:' || product_rec.product_stock);
        DBMS_OUTPUT.PUT_LINE('Product Price: ' || product_rec.product_price);
        DBMS_OUTPUT.PUT_LINE('Product Inserted:' || product_rec.product_created);
        DBMS_OUTPUT.PUT_LINE('Product Updated:' || product_rec.product_updated);
        DBMS_OUTPUT.PUT_LINE('---');
    END LOOP;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Product Not Found'||p_product_name);
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
  END get_products_by_category;

END pkg_e_commerce_website;
