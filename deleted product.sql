PROCEDURE delete_product(
    p_product_name      products.product_name%type) IS
  BEGIN
     DELETE FROM products WHERE product_name=p_product_name;
   DBMS_OUTPUT.PUT_LINE('Deleted Product');
   COMMIT;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Product Not Found');
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error:' || SQLERRM);
      ROLLBACK;
  END delete_product;
