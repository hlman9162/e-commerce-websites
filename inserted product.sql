CREATE OR REPLACE PACKAGE BODY pkg_e_commerce_website AS
  PROCEDURE insert_product(
    p_product_name      products.product_name%type,
    p_product_category  products.product_category%type,
    p_product_size      products.product_size%type,
    p_product_stock     products.product_stock%type,
    p_product_price     products.product_price%type,
    p_order_id          orders_log.order_id%type) IS
  BEGIN
    INSERT INTO products (
      product_name,
      product_category,
      product_size,
      product_stock,
      product_price,
      product_created)
    VALUES (
      p_product_name,
      p_product_category,
      p_product_size,
      p_product_stock,
      p_product_price,
      SYSTIMESTAMP);
    INSERT INTO orders_log (
      order_id,
      log_timestamp,
      log_message)
    VALUES (
      p_order_id,
      systimestamp,
      'Product Inserted');
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Error:' || SQLERRM);
      ROLLBACK;
  END insert_product;
