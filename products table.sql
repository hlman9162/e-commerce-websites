CREATE TABLE products(
 id               number GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
 product_name     varchar2(100) NOT NULL,
 product_category varchar2(100) NOT NULL,
 product_size     varchar2(10),
 product_stock    number,
 product_price    number,
 product_created  timestamp,
 product_updated  timestamp);
 
